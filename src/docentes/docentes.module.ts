import { Module } from '@nestjs/common';
import { DocenteController } from './controllers/docente.controller';
import { SharedModule } from '@/shared/shared.module';
import { DocenteService } from './services/docente.service';
import { DocentesNeuralactionsService } from '@/docentes/services/docentes-neuralactions.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DocenteMateria } from '@/docentes/entities/docente-materia.entity';

@Module({
  controllers: [DocenteController],
  providers: [DocenteService, DocentesNeuralactionsService],
  imports: [SharedModule, TypeOrmModule.forFeature([DocenteMateria])],
  exports: [DocenteService],
})
export class DocentesModule {}
