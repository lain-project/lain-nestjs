import { BadRequestException, Injectable } from '@nestjs/common';
import { AsignarDocente } from '@/docentes/dto/docente.dto';
import { UsuarioService } from '@/shared/services/usuario.service';
import { MateriaService } from '@/shared/services/materia.service';
import { DocentesNeuralactionsService } from '@/docentes/services/docentes-neuralactions.service';
import { InjectRepository } from '@nestjs/typeorm';
import { DocenteMateria } from '@/docentes/entities/docente-materia.entity';
import { Repository } from 'typeorm';

@Injectable()
export class DocenteService {
  constructor(
    @InjectRepository(DocenteMateria)
    private readonly docenteMateriaRepository: Repository<DocenteMateria>,
    private readonly usuarioService: UsuarioService,
    private readonly materiaService: MateriaService,
    private readonly docenteNeuralactions: DocentesNeuralactionsService,
  ) {}

  async asignarDocente(
    materiaId: string,
    { idUsuario }: AsignarDocente,
  ): Promise<any> {
    const materia = await this.materiaService.findOne(materiaId);
    const docente = await this.usuarioService.obtenerDocente(idUsuario);
    if (!materia || !docente) {
      throw new BadRequestException('No se encontró la materia o el docente');
    }
    const nodoId = await this.docenteNeuralactions.guardarDocente(
      docente,
      materia,
    );

    const docenteMateria = await this.docenteMateriaRepository.findOne({
      where: { materia, docente },
    });
    if (docenteMateria) {
      docenteMateria.estado = true;
      docenteMateria.nodoId = nodoId;
      await this.docenteMateriaRepository.save(docenteMateria);
    } else {
      const docenteMateria = await this.docenteMateriaRepository.create({
        materia,
        docente,
        nodoId,
      });
      await this.docenteMateriaRepository.save(docenteMateria);
    }

    await this.docenteNeuralactions.relacionarDocenteEquipo(nodoId, materia);
    return true;
  }

  async getDocente(materiaId: string): Promise<any> {
    const materia = await this.materiaService.findOne(materiaId);
    const docentesMateria = await this.docenteMateriaRepository.find({
      where: { materia, estado: true },
      relations: ['docente', 'docente.perfil', 'materia'],
    });

    const docentes = docentesMateria.map(({ docente }) => ({
      ...docente,
    }));

    return docentes;
  }

  async desasignarDocente(materiaId: string, docenteId: number) {
    const docenteMateria = await this.docenteMateriaRepository.findOne({
      where: { materia: { id: materiaId }, docente: { id: docenteId } },
      relations: ['docente', 'materia'],
    });

    docenteMateria.estado = false;
    await this.docenteNeuralactions.eliminarDocente(docenteMateria.nodoId);
    await this.docenteMateriaRepository.save(docenteMateria);
    return true;
  }

  async obtenerDocentesNoAsignadosAMateria(idMateria: string) {
    const docentes = await this.usuarioService.obtenerDocentes();
    const docentesAsignados = await this.docenteMateriaRepository.find({
      where: { materia: { id: idMateria }, estado: true },
      relations: ['docente', 'materia'],
    });

    const idDocentesAsignados = docentesAsignados.map(
      (docente) => docente.docente.id,
    );

    const docentesNoAsignados = docentes.filter(
      (docente) => !idDocentesAsignados.find((id) => docente.id === id),
    );

    return docentesNoAsignados;
  }

  async obtenerAsignaciones(idDocente: number) {
    const docente = await this.usuarioService.obtenerDocente(idDocente);
    return await this.docenteMateriaRepository.find({
      where: { docente },
      relations: [
        'materia',
        'materia.anioPlanEstudio',
        'materia.anioPlanEstudio.planDeEstudio',
      ],
    });
  }
}
