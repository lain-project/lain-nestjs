import { Injectable } from '@nestjs/common';
import { NeuralactionsService } from '@/shared/services/neuralactions.service';
import { Usuario } from '@/shared/entities/usuario.entity';
import { Materia } from '@/shared/entities/materia.entity';
import { Nodo } from '@/shared/domain/nodo.model';
import { EsquemaService } from '@/shared/services/esquema.service';
import { NodoMateriaService } from '@/shared/services/nodo-materia.service';
import { RelacionNeuralService } from '@/shared/services/relacion-neural.service';

@Injectable()
export class DocentesNeuralactionsService {
  constructor(
    private readonly neuralactions: NeuralactionsService,
    private readonly esquemaService: EsquemaService,
    private readonly nodoMateriaService: NodoMateriaService,
    private readonly relacionNeuralService: RelacionNeuralService,
  ) {}

  async guardarDocente(docente: Usuario, materia: Materia): Promise<string> {
    const planDeEstudio = materia.anioPlanEstudio.planDeEstudio;
    const esquemaDocente = await this.esquemaService.findByPlanDeEstudio(
      planDeEstudio.id,
      'docente',
    );
    const nodo: Nodo = {
      values: {
        nombres: docente.perfil.nombres,
        name: docente.perfil.apellidos + ' ' + docente.perfil.nombres,
        apellidos: docente.perfil.apellidos,
        email: docente.email,
        cargo: docente.perfil.cargo,
        scheme_id: esquemaDocente.esquemaId,
      },
      workspaces: {
        active_workspace_id: planDeEstudio.workspaceId,
      },
    };
    return await this.neuralactions.crearNodo(nodo);
  }

  async relacionarDocenteEquipo(nodoDocenteId: string, materia: Materia) {
    const nodoEquipo = await this.nodoMateriaService.obtenerNodoMateria(
      materia.id,
      'Equipo de trabajo',
    );

    const idRelacion = await this.relacionNeuralService.crearRelacion(
      nodoDocenteId,
      nodoEquipo.nodoId,
      'pertenece a',
    );
    return await this.relacionNeuralService.guardarRelacion(
      idRelacion,
      nodoDocenteId,
      nodoEquipo.nodoId,
    );
  }

  async eliminarDocente(nodoId: string) {
    return await this.neuralactions.eliminarNodo(nodoId);
  }
}
