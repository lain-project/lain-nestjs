import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Audit } from '@/shared/entities/audit.entity';
import { Usuario } from '@/shared/entities/usuario.entity';
import { Materia } from '@/shared/entities/materia.entity';

@Entity('docentes_materias')
export class DocenteMateria extends Audit {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { name: 'nodo_id' })
  nodoId: string;

  @Column('boolean', { name: 'estado', default: true })
  estado: boolean;

  @ManyToOne(() => Usuario)
  @JoinColumn({ name: 'docente_id' })
  docente: Usuario;

  @ManyToOne(() => Materia)
  @JoinColumn({ name: 'materia_id' })
  materia: Materia;
}
