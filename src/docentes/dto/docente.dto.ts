import { IsNotEmpty, IsPositive } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { NeuralactionsToken } from '@/shared/dtos/neuralactions-token';

export class AsignarDocente extends NeuralactionsToken {
  @IsPositive()
  @IsNotEmpty()
  @ApiProperty()
  readonly idUsuario: number;
}
