import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AsignarDocente } from '@/docentes/dto/docente.dto';
import { DocenteService } from '@/docentes/services/docente.service';
import { Roles } from '@/auth/decorators/roles.decorator';
import { Rol } from '@/auth/models/rol.enum';
import { JwtAuthGuard } from '@/auth/guards/jwt-auth.guard';
import { RolesGuard } from '@/auth/guards/roles.guard';

@UseGuards(JwtAuthGuard, RolesGuard)
@ApiTags(`Docentes`)
@Controller('docentes')
export class DocenteController {
  constructor(private readonly docenteService: DocenteService) {}

  @Roles(Rol.ADMIN)
  @Post('/:materiaId')
  async asignarDocente(
    @Param('materiaId') materiaId: string,
    @Body() docenteAsignar: AsignarDocente,
  ) {
    return await this.docenteService.asignarDocente(materiaId, docenteAsignar);
  }

  @Roles(Rol.DOCENTE, Rol.ADMIN)
  @Get('/:materiaId')
  async getDocente(@Param('materiaId') materiaId: string) {
    return await this.docenteService.getDocente(materiaId);
  }

  @Roles(Rol.ADMIN)
  @Delete('/:materiaId/:docenteId')
  async desasignarDocente(
    @Param('materiaId') materiaId: string,
    @Param('docenteId') docenteId: number,
  ) {
    return await this.docenteService.desasignarDocente(materiaId, docenteId);
  }

  @Roles(Rol.ADMIN)
  @Get('/:materiaId/no-asignados')
  async obtenerDocentesNoAsignados(@Param('materiaId') materiaId: string) {
    return await this.docenteService.obtenerDocentesNoAsignadosAMateria(
      materiaId,
    );
  }

}
