import { Module } from '@nestjs/common';
import { ContenidoController } from './controllers/contenido.controller';
import { ContenidoService } from './services/contenido.service';
import { SharedModule } from '@/shared/shared.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Unidad } from '@/unidad/entities/unidad.entity';
import { Contenido } from '@/contenido/entities/contenido.entity';
import { ContenidoNeuralactionsService } from './services/contenido-neuralactions.service';
import { UnidadModule } from '@/unidad/unidad.module';

@Module({
  controllers: [ContenidoController],
  providers: [ContenidoService, ContenidoNeuralactionsService],
  imports: [
    SharedModule,
    UnidadModule,
    TypeOrmModule.forFeature([Contenido, Unidad]),
  ],
})
export class ContenidoModule {}
