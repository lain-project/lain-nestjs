import { Audit } from '@/shared/entities/audit.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Unidad } from '@/unidad/entities/unidad.entity';

@Entity('contenidos')
export class Contenido extends Audit {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { name: 'nodo_id' })
  nodoId: string;

  @Column('varchar', { name: 'nombre' })
  nombre: string;

  @Column('varchar', { name: 'descripcion' })
  descripcion: string;

  @Column('boolean', { name: 'estado', default: true })
  estado: boolean;

  @Column('boolean', { name: 'minimo', default: true })
  esMinimo: boolean;

  @ManyToOne(() => Unidad)
  @JoinColumn({ name: 'unidad_id' })
  unidad: Unidad;
}
