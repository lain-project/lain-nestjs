import { NeuralactionsToken } from '@/shared/dtos/neuralactions-token';
import { ApiProperty, PartialType } from '@nestjs/swagger';
import {IsBoolean, IsOptional, IsString} from 'class-validator';

export class ContenidoACrear extends NeuralactionsToken {
  @IsString()
  @IsOptional()
  @ApiProperty()
  nombre: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  descripcion: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty()
  esMinimo: boolean;

  @IsString()
  @IsOptional()
  @ApiProperty()
  unidadId: string;
}

export class ContenidoAActualizar extends PartialType(ContenidoACrear) {
  @IsString()
  @IsOptional()
  @ApiProperty()
  id: string;
}
