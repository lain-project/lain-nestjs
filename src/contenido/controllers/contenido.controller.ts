import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { Roles } from '@/auth/decorators/roles.decorator';
import { Rol } from '@/auth/models/rol.enum';
import { ContenidoService } from '@/contenido/services/contenido.service';
import {
  ContenidoAActualizar,
  ContenidoACrear,
} from '@/contenido/dto/contenido.dto';
import { JwtAuthGuard } from '@/auth/guards/jwt-auth.guard';
import { RolesGuard } from '@/auth/guards/roles.guard';
import { ApiTags } from '@nestjs/swagger';

@UseGuards(JwtAuthGuard, RolesGuard)
@ApiTags(`Contenido`)
@Controller('contenido')
export class ContenidoController {
  constructor(private readonly contenidoService: ContenidoService) {}

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Get('/:unidadId')
  async obtenerContenidos(@Param('unidadId') unidadId: string) {
    return await this.contenidoService.obtenerContenidoPorUnidad(unidadId);
  }

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Post('')
  async asignarContenidos(
    @Body() contenidoACrear: ContenidoACrear,
  ): Promise<boolean> {
    return await this.contenidoService.asignarContenido(contenidoACrear);
  }

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Put('')
  async actualizarContenidos(
    @Body() contenidoAActualizar: ContenidoAActualizar,
  ) {
    return await this.contenidoService.actualizarContenido(
      contenidoAActualizar,
    );
  }

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Delete('/:contenidoId')
  async eliminarContenido(@Param('contenidoId') contenidoId: string) {
    return await this.contenidoService.eliminarContenido(contenidoId);
  }
}
