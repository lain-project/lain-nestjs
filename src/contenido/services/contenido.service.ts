import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Contenido } from '@/contenido/entities/contenido.entity';
import { Repository } from 'typeorm';
import {
  ContenidoAActualizar,
  ContenidoACrear,
} from '@/contenido/dto/contenido.dto';
import { ContenidoNeuralactionsService } from '@/contenido/services/contenido-neuralactions.service';
import { UnidadService } from '@/unidad/services/unidad.service';

@Injectable()
export class ContenidoService {
  constructor(
    @InjectRepository(Contenido)
    private readonly contenidoRepository: Repository<Contenido>,
    private readonly contenidoNeuralactionsService: ContenidoNeuralactionsService,
    private readonly unidadService: UnidadService,
  ) {}

  async obtenerContenidoPorUnidad(unidadId: string) {
    return await this.contenidoRepository.find({
      where: {
        estado: true,
        unidad: { id: unidadId },
      },
    });
  }

  async asignarContenido(contenidoACrear: ContenidoACrear): Promise<boolean> {
    const unidad = await this.unidadService.obtenerUnidad(
      contenidoACrear.unidadId,
    );
    const contenido = this.contenidoRepository.create({
      ...contenidoACrear,
      unidad,
    });

    const nodoId = await this.contenidoNeuralactionsService.guardar(
      contenido,
      unidad,
    );

    contenido.nodoId = nodoId;

    const relacion = await this.contenidoNeuralactionsService.relacionarAUnidad(
      contenido,
      unidad,
    );

    await this.contenidoRepository.save(contenido);

    return !!relacion;
  }

  async actualizarContenido(contenidoAActualizar: ContenidoAActualizar) {
    let contenido = await this.contenidoRepository.findOne({
      where: { id: contenidoAActualizar.id, estado: true },
      relations: ['unidad'],
    });

    if (!contenido) {
      throw new BadRequestException(
        `No existe contenido con el id ${contenidoAActualizar.id}`,
      );
    }

    contenido = this.contenidoRepository.merge(contenido, contenidoAActualizar);
    const unidad = await this.unidadService.obtenerUnidad(
      contenidoAActualizar.unidadId,
    );
    await this.contenidoNeuralactionsService.actualizar(unidad, contenido);
    contenido = await this.contenidoRepository.save(contenido);

    return contenido;
  }

  async eliminarContenido(id: string) {
    const contenido = await this.contenidoRepository.findOneBy({
      id,
      estado: true,
    });
    contenido.estado = false;
    await this.contenidoNeuralactionsService.eliminar(contenido.nodoId);
    return await this.contenidoRepository.save(contenido);
  }
}
