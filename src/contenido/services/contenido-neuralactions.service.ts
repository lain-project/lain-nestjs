import { Injectable } from '@nestjs/common';
import { Contenido } from '@/contenido/entities/contenido.entity';
import { Unidad } from '@/unidad/entities/unidad.entity';
import { NeuralactionsService } from '@/shared/services/neuralactions.service';
import { EsquemaService } from '@/shared/services/esquema.service';
import { RelacionNeuralService } from '@/shared/services/relacion-neural.service';
import { EsquemaEnum } from '@/shared/domain/esquema.enum';
import { Nodo } from '@/shared/domain/nodo.model';

@Injectable()
export class ContenidoNeuralactionsService {
  constructor(
    private readonly neuralactions: NeuralactionsService,
    private readonly esquemaService: EsquemaService,
    private readonly relacionNeuralService: RelacionNeuralService,
  ) {}

  async guardar(contenido: Contenido, unidad: Unidad): Promise<string> {
    const { planDeEstudio } = unidad.materia.anioPlanEstudio;
    const { esquemaId } = await this.esquemaService.findByPlanDeEstudio(
      planDeEstudio.id,
      EsquemaEnum.CONTENIDO,
    );
    // TODO: ELEGIR COLOR EN BASE A SI ES MINIMO O NO
    const nodo: Nodo = {
      values: {
        unidad: unidad.nombre,
        name: contenido.nombre,
        nombre: contenido.nombre,
        descripcion: contenido.descripcion,
        scheme_id: esquemaId,
        hue: 123,
      },
      workspaces: {
        active_workspace_id: planDeEstudio.workspaceId,
      },
    };

    return await this.neuralactions.crearNodo(nodo);
  }

  async relacionarAUnidad(contenido: Contenido, unidad: Unidad) {
    const idRelacion = await this.relacionNeuralService.crearRelacion(
      contenido.nodoId,
      unidad.nodoId,
      `es contenido ${contenido.esMinimo ? 'minimo' : ''}`,
    );

    return await this.relacionNeuralService.guardarRelacion(
      idRelacion,
      contenido.nodoId,
      unidad.nodoId,
    );
  }

  async actualizar(unidad: Unidad, contenido: Contenido) {
    const { planDeEstudio } = unidad.materia.anioPlanEstudio;
    const { esquemaId } = await this.esquemaService.findByPlanDeEstudio(
      planDeEstudio.id,
      EsquemaEnum.CONTENIDO,
    );
    // TODO: ELEGIR COLOR EN BASE A SI ES MINIMO O NO
    const nodo: Nodo = {
      id: contenido.nodoId,
      values: {
        name: contenido.nombre,
        nombre: contenido.nombre,
        descripcion: contenido.descripcion,
        scheme_id: esquemaId,
        hue: 123,
      },
      workspaces: {
        active_workspace_id: planDeEstudio.workspaceId,
      },
    };

    const contenidoActualizado = await this.neuralactions.actualizarNodo(nodo);
    await this.relacionNeuralService.actualizar(
      contenido.nodoId,
      unidad.nodoId,
      `es contenido ${contenido.esMinimo ? 'minimo' : ''}`,
    );
    return contenidoActualizado;
  }

  async eliminar(nodoId: string) {
    const relaciones = await this.relacionNeuralService.eliminarRelaciones(
      nodoId,
    );
    return await this.neuralactions.eliminarNodo(nodoId);
  }
}
