import {Body, Controller, Get, Param, Post, UseGuards} from '@nestjs/common';
import { JwtAuthGuard } from '@/auth/guards/jwt-auth.guard';
import { RolesGuard } from '@/auth/guards/roles.guard';
import { ApiTags } from '@nestjs/swagger';
import { PlanDeEstudioService } from '@/plan-de-estudio/services/plan-de-estudio.service';
import { Roles } from '@/auth/decorators/roles.decorator';
import { Rol } from '@/auth/models/rol.enum';

@UseGuards(JwtAuthGuard, RolesGuard)
@ApiTags('Plan de Estudio')
@Controller('plan-de-estudio')
export class PlanDeEstudioController {
  constructor(private readonly planDeEstudioService: PlanDeEstudioService) {}

  @Roles(Rol.DOCENTE, Rol.ADMIN)
  @Get('/:idUsuario')
  async obtenerPlanesDeEstudioAsignados(@Param('idUsuario') idUsuario: number) {
    return await this.planDeEstudioService.obtenerPlanDeEstudioAsignado(
      idUsuario,
    );
  }

  @Roles(Rol.ADMIN)
  @Post('/export')
  async exportarDatos(@Body() data: any) {
    return await this.planDeEstudioService.exportarDatos(data);
  }
}
