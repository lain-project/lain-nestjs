import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PlanDeEstudio } from '@/shared/entities/plan-de-estudio.entity';
import { Repository } from 'typeorm';
import { DocenteService } from '@/docentes/services/docente.service';
import _ from 'lodash';
import shell from 'shelljs';
import cp from 'child_process';
import { MateriaService } from '@/shared/services/materia.service';
import { Materia } from '@/shared/entities/materia.entity';
import { HttpService } from '@nestjs/axios';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class PlanDeEstudioService {
  constructor(
    @InjectRepository(PlanDeEstudio)
    private readonly planDeEstudioRepository: Repository<PlanDeEstudio>,
    private readonly docenteMateria: DocenteService,
    private readonly materiaService: MateriaService,
    private readonly http: HttpService,
  ) {}

  async obtenerPlanDeEstudioAsignado(idUsuario: number) {
    const asignaciones = await this.docenteMateria.obtenerAsignaciones(
      idUsuario,
    );
    const materias = asignaciones.map((asignacion) => asignacion.materia);
    const planes = materias.map(
      (materia) => materia.anioPlanEstudio.planDeEstudio,
    );

    return _.uniqBy(planes, 'id');
  }

  async exportarDatos(data: any) {
    // const script = cp.execSync(
    //   'sh /home/deathslayer/code/lain/lain-nestjs/src/plan-de-estudio/services/temp/export.sh temp',
    // );
    const entidades: any[] = data.entidades;
    const entityData = this.extraerDatos(entidades);
    const materiasDB = await this.materiaService.getMaterias();
    const planesDB = await this.planDeEstudioRepository.find();

    const materiasResult = materiasDB.filter(
      (materia) => !entityData.materias.includes(materia.nombre),
    );
    const planesResult = planesDB.filter(
      (plan) => !entityData.planesDeEstudio.includes(plan.codigo),
    );

    const planesEntity = planesResult.map((plan) => ({
      value: plan.codigo,
      synonyms: this.generadorDeSinonimosPlan(plan),
    }));

    const materiaEntity = materiasResult.map((materia) => ({
      value: materia.nombre,
      synonyms: this.generadorDeSinonimosMateria(materia),
    }));

    //
    // script.stdout.on('data', (data) => {
    //   console.log(data);
    //   // do whatever you want here with data
    // });
    // script.stderr.on('data', (data) => {
    //   console.error(data);
    // });
    return await this.guardarEntidades(materiaEntity, planesEntity, entityData);
  }

  extraerDatos(entidades: any[]) {
    const entidadMateria = entidades.find(
      (entidad) => entidad.displayName === 'materia',
    );
    const entidadPlanDeEstudio = entidades.find(
      (entidad: any) => entidad.displayName === 'planDeEstudio',
    );

    const materias = entidadMateria.entities.map(
      (entidad: any) => entidad.value,
    );
    const planesDeEstudio = entidadPlanDeEstudio.entities.map(
      (entidad: any) => entidad.value,
    );

    return {
      materias,
      planesDeEstudio,
      entidadMateria,
      entidadPlanDeEstudio,
    };
  }

  generadorDeSinonimosPlan(plan: PlanDeEstudio) {
    return [
      plan.codigo,
      plan.nombre,
      `${plan.codigo} ${plan.nombre}`,
      `${plan.codigo} ${plan.nombre}`.toLowerCase(),
      plan.nombre.toLowerCase(),
      plan.nombre
        .split(' ')
        .map((word) => word.substring(0, 3))
        .join(' '),
    ];
  }

  generadorDeSinonimosMateria(materia: Materia) {
    return [
      materia.nombre,
      materia.nombre.toLowerCase(),
      materia.nombre.toUpperCase(),
      materia.nombre.substring(0, 3) +
        materia.nombre.substring(3, materia.nombre.length),
      materia.nombre.split(' ')[0],
    ];
  }

  async guardarEntidades(
    materia: any,
    planes: any,
    { entidadMateria, entidadPlanDeEstudio }: any,
  ): Promise<any> {
    entidadMateria.entities.push(...materia);
    entidadPlanDeEstudio.entities.push(...planes);
    const entities = [entidadMateria, entidadPlanDeEstudio];
    const response = await lastValueFrom(
      this.http.post(`http://localhost:3002/entidades`, entities),
    );
    console.log(response.data);
    return response.data;
  }
}
