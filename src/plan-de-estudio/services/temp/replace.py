
result = ['DROP DATABASE lain;', 'CREATE DATABASE lain;']
with open('dump.sql') as f:
    lines = f.readlines()
    for line in lines:
        if 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;' in line:
            print('Create extension')
        elif 'COMMENT ON EXTENSION "uuid-ossp"' in line:
            print('COMMENT on')
        elif 'public.uuid_generate_v4()' in line:
            l = line.replace('public.uuid_generate_v4()', 'gen_random_uuid()')
            result.append(l)
        elif 'OWNER TO postgres' in line:
            l = line.replace('OWNER TO postgres', 'OWNER TO himuraxkenji')
            result.append(l)
        else:
            result.append(line)

with open('result.sql', 'w') as f:
    for r in result:
        f.write(r)
    
