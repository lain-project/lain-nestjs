import { Module } from '@nestjs/common';
import { PlanDeEstudioController } from './controllers/plan-de-estudio.controller';
import { PlanDeEstudioService } from './services/plan-de-estudio.service';
import { SharedModule } from '@/shared/shared.module';
import { DocentesModule } from '@/docentes/docentes.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlanDeEstudio } from '@/shared/entities/plan-de-estudio.entity';
import { HttpModule } from '@nestjs/axios';

@Module({
  controllers: [PlanDeEstudioController],
  providers: [PlanDeEstudioService],
  imports: [
    SharedModule,
    DocentesModule,
    TypeOrmModule.forFeature([PlanDeEstudio]),
    HttpModule,
  ],
})
export class PlanDeEstudioModule {}
