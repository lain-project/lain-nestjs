import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService) {}

  async validateUser(email: string, password: string): Promise<boolean> {
    // buscar por email
    return true;
  }

  async generateJTW(user: any) {
    const payload = {
      user: {
        username: 'himura',
        email: 'aaron.ariperto@gmail.com',
        avatar: 'img',
        rolname: 'ADMIN',
        google: false,
      },
    };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
