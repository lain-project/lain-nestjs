import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '@/auth/guards/jwt-auth.guard';
import { RolesGuard } from '@/auth/guards/roles.guard';
import { ApiTags } from '@nestjs/swagger';
import { Roles } from '@/auth/decorators/roles.decorator';
import { Rol } from '@/auth/models/rol.enum';
import { PropuestaService } from '@/propuesta/services/propuesta.service';
import {
  ActualizarPropuesta,
  CrearPropuesta,
} from '@/propuesta/dto/propuesta.dto';

@UseGuards(JwtAuthGuard, RolesGuard)
@ApiTags(`Propuestas`)
@Controller('propuesta')
export class PropuestaController {
  constructor(private readonly propuestaService: PropuestaService) {}

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Post('/')
  async asignarPropuesta(@Body() propuesta: CrearPropuesta): Promise<boolean> {
    return await this.propuestaService.asignarPropuesta(propuesta);
  }

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Put('/')
  async actualizarPropuesta(
    @Body() propuesta: ActualizarPropuesta,
  ): Promise<boolean> {
    return await this.propuestaService.actualizarPropuesta(propuesta);
  }

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Get('/:materiaId')
  async obtenerPropuesta(@Param('materiaId') materiaId: string) {
    return await this.propuestaService.obtenerPropuesta(materiaId);
  }
}
