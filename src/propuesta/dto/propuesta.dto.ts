import { NeuralactionsToken } from '@/shared/dtos/neuralactions-token';
import { IsOptional, IsString } from 'class-validator';
import { ApiProperty, PartialType } from '@nestjs/swagger';

export class CrearPropuesta extends NeuralactionsToken {
  @IsString()
  @IsOptional()
  @ApiProperty()
  contenidoMinimo: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  fundamentos: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  objetivos: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  metodologia: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  evaluacion: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  bibliografiaGeneral: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  materiaId: string;
}

export class ActualizarPropuesta extends PartialType(CrearPropuesta) {
  @IsString()
  @IsOptional()
  @ApiProperty()
  id: string;
}
