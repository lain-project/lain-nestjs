import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Materia } from '@/shared/entities/materia.entity';
import { Audit } from '@/shared/entities/audit.entity';

@Entity('propuestas')
export class Propuesta extends Audit {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { name: 'nodo_id' })
  nodoId: string;

  @Column('boolean', { name: 'estado', default: true })
  estado: boolean;

  @Column('varchar', { name: 'contenido_minimo' })
  contenidoMinimo: string;

  @Column('varchar', { name: 'fundamentos' })
  fundamentos: string;

  @Column('varchar', { name: 'objetivos' })
  objetivos: string;

  @Column('varchar', { name: 'metodologia' })
  metodologia: string;

  @Column('varchar', { name: 'evaluacion' })
  evaluacion: string;

  @Column('varchar', { name: 'bibliografia_general' })
  bibliografiaGeneral: string;

  @Column('varchar', { name: 'materia_id' })
  materiaId: string;

  @ManyToOne(() => Materia)
  @JoinColumn({ name: 'materia_id' })
  materia: Materia;
}
