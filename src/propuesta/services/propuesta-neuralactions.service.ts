import { Injectable } from '@nestjs/common';
import { NeuralactionsService } from '@/shared/services/neuralactions.service';
import { EsquemaService } from '@/shared/services/esquema.service';
import { Materia } from '@/shared/entities/materia.entity';
import { Propuesta } from '@/propuesta/entities/propuesta.entity';
import { Nodo } from '@/shared/domain/nodo.model';
import { RelacionNeuralService } from '@/shared/services/relacion-neural.service';

@Injectable()
export class PropuestaNeuralactionsService {
  constructor(
    private readonly neuralactions: NeuralactionsService,
    private readonly esquemaService: EsquemaService,
    private readonly relacionNeuralService: RelacionNeuralService,
  ) {}

  async guardarPropuesta(
    propuesta: Propuesta,
    materia: Materia,
  ): Promise<string> {
    const { planDeEstudio } = materia.anioPlanEstudio;
    const esquemaPropuesta = await this.esquemaService.findByPlanDeEstudio(
      planDeEstudio.id,
      'programa',
    );

    const nodo: Nodo = {
      values: {
        name: 'Propuesta',
        contenido_minimo: propuesta.contenidoMinimo,
        bibliografia_general: propuesta.bibliografiaGeneral,
        metodologia: propuesta.metodologia,
        evaluacion: propuesta.evaluacion,
        fundamentos: propuesta.fundamentos,
        objetivos: propuesta.objetivos,
        scheme_id: esquemaPropuesta.esquemaId,
        size: 3,
        hue: 152,
      },
      workspaces: {
        active_workspace_id: planDeEstudio.workspaceId,
      },
    };
    return await this.neuralactions.crearNodo(nodo);
  }

  async relacionarPropuestaMateria(nodoPropuestaId: string, materia: Materia) {
    const idRelacion = await this.relacionNeuralService.crearRelacion(
      nodoPropuestaId,
      materia.nodoId,
      'pertenece a',
    );
    return await this.relacionNeuralService.guardarRelacion(
      idRelacion,
      nodoPropuestaId,
      materia.nodoId,
    );
  }

  async actualizarPropuesta(
    propuesta: Propuesta,
    materia: Materia,
  ): Promise<string> {
    const { planDeEstudio } = materia.anioPlanEstudio;
    const esquemaPropuesta = await this.esquemaService.findByPlanDeEstudio(
      planDeEstudio.id,
      'programa',
    );

    const nodo: Nodo = {
      id: propuesta.nodoId,
      values: {
        name: 'Propuesta',
        contenido_minimo: propuesta.contenidoMinimo,
        bibliografia_general: propuesta.bibliografiaGeneral,
        metodologia: propuesta.metodologia,
        evaluacion: propuesta.evaluacion,
        fundamentos: propuesta.fundamentos,
        objetivos: propuesta.objetivos,
        scheme_id: esquemaPropuesta.esquemaId,
        size: 3,
        hue: 152,
      },
      workspaces: {
        active_workspace_id: planDeEstudio.workspaceId,
      },
    };
    return await this.neuralactions.actualizarNodo(nodo);
  }
}
