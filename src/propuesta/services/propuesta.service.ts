import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Propuesta } from '@/propuesta/entities/propuesta.entity';
import { Repository } from 'typeorm';
import { MateriaService } from '@/shared/services/materia.service';
import {
  ActualizarPropuesta,
  CrearPropuesta,
} from '@/propuesta/dto/propuesta.dto';
import { PropuestaNeuralactionsService } from '@/propuesta/services/propuesta-neuralactions.service';

@Injectable()
export class PropuestaService {
  constructor(
    @InjectRepository(Propuesta)
    private readonly propuestaRepository: Repository<Propuesta>,
    private readonly propuestaNeuralactionsService: PropuestaNeuralactionsService,
    private readonly materiaService: MateriaService,
  ) {}

  async asignarPropuesta(propuestaACrear: CrearPropuesta): Promise<boolean> {
    const { materiaId } = propuestaACrear;
    const materia = await this.materiaService.findOne(materiaId);
    const propuestaMateria = await this.propuestaRepository.findOne({
      where: { materia: { id: materiaId } },
    });

    if (propuestaMateria) {
      throw new BadRequestException(
        `Ya existe una propuesta para la materia ${materia.nombre}`,
      );
    }

    const propuesta = await this.propuestaRepository.create({
      ...propuestaACrear,
      materia,
    });

    const nodoId = await this.propuestaNeuralactionsService.guardarPropuesta(
      propuesta,
      materia,
    );

    propuesta.nodoId = nodoId;

    await this.propuestaRepository.save(propuesta);

    const relacion =
      await this.propuestaNeuralactionsService.relacionarPropuestaMateria(
        nodoId,
        materia,
      );

    return !!relacion;
  }

  async actualizarPropuesta(
    propuestaAActualizar: ActualizarPropuesta,
  ): Promise<boolean> {
    let propuesta = await this.propuestaRepository.findOne({
      where: { id: propuestaAActualizar.id },
      relations: ['materia'],
    });
    if (!propuesta) {
      throw new BadRequestException(
        `No existe materia con el id ${propuesta.id}`,
      );
    }
    propuesta = this.propuestaRepository.merge(propuesta, propuestaAActualizar);
    const materia = await this.materiaService.findOne(propuesta.materia.id);
    await this.propuestaNeuralactionsService.actualizarPropuesta(
      propuesta,
      materia,
    );
    propuesta = await this.propuestaRepository.save(propuesta);
    return !!propuesta;
  }

  async obtenerPropuesta(materiaId: string) {
    return await this.propuestaRepository.findOne({
      where: { materia: { id: materiaId } },
    });
  }
}
