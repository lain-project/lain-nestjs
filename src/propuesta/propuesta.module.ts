import { Module } from '@nestjs/common';
import { PropuestaController } from './controllers/propuesta.controller';
import { PropuestaService } from './services/propuesta.service';
import { SharedModule } from '@/shared/shared.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Propuesta } from '@/propuesta/entities/propuesta.entity';
import { Materia } from '@/shared/entities/materia.entity';
import { PropuestaNeuralactionsService } from './services/propuesta-neuralactions.service';

@Module({
  controllers: [PropuestaController],
  providers: [PropuestaService, PropuestaNeuralactionsService],
  imports: [SharedModule, TypeOrmModule.forFeature([Propuesta, Materia])],
})
export class PropuestaModule {}
