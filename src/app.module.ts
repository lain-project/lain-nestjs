import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DocentesModule } from './docentes/docentes.module';
import { DatabaseModule } from './database/database.module';
import { ConfigModule } from '@nestjs/config';
import { enviroments } from './enviroments';
import { SharedModule } from './shared/shared.module';
import { AuthModule } from './auth/auth.module';
import { PropuestaModule } from './propuesta/propuesta.module';
import config from './config';
import Joi from 'joi';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { UnidadModule } from './unidad/unidad.module';
import { ContenidoModule } from './contenido/contenido.module';
import { BibliografiaModule } from './bibliografia/bibliografia.module';
import { CorrelativaModule } from './correlativa/correlativa.module';
import { PlanDeEstudioModule } from './plan-de-estudio/plan-de-estudio.module';
import { ReportesModule } from './reportes/reportes.module';
@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'client'), // <-- path to the static files
    }),
    ConfigModule.forRoot({
      envFilePath: enviroments[process.env.NODE_ENV] || '.env',
      load: [config],
      isGlobal: true,
      validationSchema: Joi.object({
        DATABASE_HOST: Joi.string().required(),
        DATABASE_NAME: Joi.string().required(),
        DATABASE_PASSWORD: Joi.string().required(),
        DATABASE_PORT: Joi.number().required(),
        DATABASE_USER: Joi.string().required(),
      }),
    }),
    DocentesModule,
    DatabaseModule,
    SharedModule,
    AuthModule,
    PropuestaModule,
    UnidadModule,
    ContenidoModule,
    BibliografiaModule,
    CorrelativaModule,
    PlanDeEstudioModule,
    ReportesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
