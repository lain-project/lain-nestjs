import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class NeuralactionsToken {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly neuralactionsToken: string;
}
