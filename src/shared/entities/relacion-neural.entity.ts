import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('relaciones_neural')
export class RelacionNeural {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { name: 'nodo_inicial' })
  nodoInicial: string;

  @Column('varchar', { name: 'nodo_final' })
  nodoFinal: string;
}
