import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('years')
export class Anio {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar', { name: 'name' })
  nombre: string;

  @Column('varchar', { name: 'numeration' })
  numeration: string;
}
