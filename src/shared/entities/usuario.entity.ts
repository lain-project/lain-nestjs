import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Estado } from '@/shared/entities/estado';
import { Rol } from '@/shared/entities/rol.entity';
import { Perfil } from '@/shared/entities/perfil.entity';
import { Exclude } from 'class-transformer';

@Entity('users')
export class Usuario {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  username: string;

  @Column('varchar')
  email: string;

  @Exclude()
  @Column('int', { name: 'status' })
  estado: Estado;

  @OneToOne(() => Rol, (rol) => rol.usuario)
  @JoinColumn({
    name: 'rol_id',
  })
  rol: Rol;

  @OneToOne(() => Perfil, (perfil) => perfil.usuario)
  @JoinColumn({
    name: 'profile_id',
  })
  perfil: Perfil;
}
