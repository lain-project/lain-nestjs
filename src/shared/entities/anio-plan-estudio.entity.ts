import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Materia } from '@/shared/entities/materia.entity';
import { PlanDeEstudio } from '@/shared/entities/plan-de-estudio.entity';
import { Anio } from '@/shared/entities/anio.entity';

@Entity('study_plans_years')
export class AnioPlanEstudio {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('int', { name: 'year_id' })
  anioId: number;

  @Column('varchar', { name: 'node_id' })
  nodeId: string;

  @ManyToOne(
    () => PlanDeEstudio,
    (planDeEstudio) => planDeEstudio.aniosPlanEstudio,
  )
  @JoinColumn({ name: 'study_plan_id' })
  planDeEstudio: PlanDeEstudio;

  @OneToMany(() => Materia, (materia) => materia.anioPlanEstudio)
  materias: Materia[];

  @ManyToOne(() => Anio)
  @JoinColumn({ name: 'year_id' })
  anio: Anio;
}
