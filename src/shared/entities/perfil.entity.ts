import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Usuario } from '@/shared/entities/usuario.entity';

@Entity('profiles')
export class Perfil {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar', { name: 'lastname' })
  apellidos: string;

  @Column('varchar', { name: 'firstname' })
  nombres: string;

  @Column('varchar', { name: 'position' })
  cargo: string;

  @OneToOne(() => Usuario, (usuario) => usuario.perfil)
  usuario: Usuario;
}
