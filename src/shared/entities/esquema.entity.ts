import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PlanDeEstudio } from '@/shared/entities/plan-de-estudio.entity';

@Entity('schemas')
export class Esquema {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar', { name: 'name' })
  nombre: string;

  @Column('varchar', { name: 'schema_id' })
  esquemaId: string;

  @ManyToOne(() => PlanDeEstudio, (planDeEstudio) => planDeEstudio.esquemas)
  @JoinColumn({ name: 'study_plan_id' })
  planDeEstudio: PlanDeEstudio;
}
