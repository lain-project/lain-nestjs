import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { AnioPlanEstudio } from '@/shared/entities/anio-plan-estudio.entity';
import { NodoMateria } from '@/shared/entities/nodo-materia.entity';
import { Propuesta } from '@/propuesta/entities/propuesta.entity';

@Entity('subjects')
export class Materia {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { name: 'name' })
  nombre: string;

  @Column('varchar', { name: 'code' })
  codigo: string;

  @Column('varchar', { name: 'regime' })
  regimen: string;

  @Column('int', { name: 'weekly_hours' })
  horasSemanales: number;

  @Column('int', { name: 'total_hours' })
  horasTotales: number;

  @Column('varchar', { name: 'node_id' })
  nodoId: string;

  @ManyToOne(
    () => AnioPlanEstudio,
    (anioPlanEstudio) => anioPlanEstudio.materias,
  )
  @JoinColumn({ name: 'study_plan_year' })
  anioPlanEstudio: AnioPlanEstudio;

  @OneToMany(() => NodoMateria, (nodoMateria) => nodoMateria.materia)
  nodosMaterias: NodoMateria[];
}
