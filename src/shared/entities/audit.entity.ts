import { Column, CreateDateColumn, Entity, UpdateDateColumn } from 'typeorm';

@Entity()
export class Audit {
  @CreateDateColumn({
    name: 'fecha_creacion',
    type: 'timestamp',
    default: () => 'NOW()',
  })
  fechaCreacion: Date;

  @UpdateDateColumn({
    name: 'fecha_actualizacion',
    type: 'timestamp',
    default: () => 'NOW()',
  })
  fechaActualizacion: Date;

  @Column('int', { name: 'usuario_creador' })
  usuarioCreador: number;

  @Column('int', { name: 'usuario_actualizador' })
  usuarioActualizador: number;
}
