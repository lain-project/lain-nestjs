import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Materia } from '@/shared/entities/materia.entity';

@Entity('nodos_materias')
export class NodoMateria {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar')
  nombre: string;

  @Column('varchar', { name: 'nodo_id' })
  nodoId: string;

  @ManyToOne(() => Materia, (materia) => materia.nodosMaterias)
  @JoinColumn({ name: 'materia_id' })
  materia: Materia;
}
