import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Esquema } from '@/shared/entities/esquema.entity';
import { AnioPlanEstudio } from '@/shared/entities/anio-plan-estudio.entity';

@Entity('study_plans')
export class PlanDeEstudio{
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar', { name: 'name' })
  nombre: string;

  @Column('varchar', { name: 'code' })
  codigo: string;

  @Column('varchar', { name: 'description' })
  descripcion: string;

  @Column('varchar', { name: 'workspace_id' })
  workspaceId: string;

  @OneToMany(() => Esquema, (esquema) => esquema.planDeEstudio)
  esquemas: Esquema[];

  @OneToMany(
    () => AnioPlanEstudio,
    (anioPlanEstudio) => anioPlanEstudio.planDeEstudio,
  )
  aniosPlanEstudio: AnioPlanEstudio[];
}
