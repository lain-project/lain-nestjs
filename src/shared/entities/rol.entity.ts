import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Estado } from '@/shared/entities/estado';
import { Usuario } from '@/shared/entities/usuario.entity';

@Entity('rols')
export class Rol {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar', { name: 'name' })
  nombre: string;

  @Column('varchar', { name: 'description' })
  descripcion: string;

  @Column('int', { name: 'status' })
  estado: Estado;

  @OneToOne(() => Usuario, (usuario) => usuario.rol)
  usuario: Usuario;
}
