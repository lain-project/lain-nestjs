import { Module } from '@nestjs/common';
import { UsuarioService } from './services/usuario.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Usuario } from '@/shared/entities/usuario.entity';
import { Rol } from '@/shared/entities/rol.entity';
import { Perfil } from '@/shared/entities/perfil.entity';
import { MateriaService } from './services/materia.service';
import { Materia } from '@/shared/entities/materia.entity';
import { AnioPlanEstudio } from '@/shared/entities/anio-plan-estudio.entity';
import { Anio } from '@/shared/entities/anio.entity';
import { Esquema } from '@/shared/entities/esquema.entity';
import { NodoMateria } from '@/shared/entities/nodo-materia.entity';
import { PlanDeEstudio } from '@/shared/entities/plan-de-estudio.entity';
import { RelacionNeural } from '@/shared/entities/relacion-neural.entity';
import { NeuralactionsService } from './services/neuralactions.service';
import { ConfigModule } from '@nestjs/config';
import { HttpModule } from '@nestjs/axios';
import { EsquemaService } from './services/esquema.service';
import { RelacionNeuralService } from './services/relacion-neural.service';
import { NodoMateriaService } from './services/nodo-materia.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Anio,
      AnioPlanEstudio,
      Esquema,
      NodoMateria,
      Materia,
      Perfil,
      PlanDeEstudio,
      RelacionNeural,
      Rol,
      Usuario,
    ]),
    ConfigModule,
    HttpModule,
  ],
  providers: [
    UsuarioService,
    MateriaService,
    NeuralactionsService,
    EsquemaService,
    RelacionNeuralService,
    NodoMateriaService,
  ],
  exports: [
    UsuarioService,
    MateriaService,
    NeuralactionsService,
    EsquemaService,
    RelacionNeuralService,
    NodoMateriaService,
  ],
})
export class SharedModule {}
