export class Nodo {
  id?: string;
  values?: any;
  workspaces?: Workspace;
}

class Workspace {
  active_workspace_id: string;
}
