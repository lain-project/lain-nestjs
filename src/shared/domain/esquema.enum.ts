export enum EsquemaEnum {
  ANIO = 'año',
  BIBLIOGRAFIA = 'bibliografia',
  CONTENIDO = 'contenido minimo',
  DOCENTE = 'docente',
  MATERIA = 'materia',
  PROGRAMA = 'programa',
  TRABAJO_PRACTICO = 'trabajo practico',
  UNIDAD = 'unidad',
}
