import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Usuario } from '@/shared/entities/usuario.entity';
import { Repository } from 'typeorm';
import { RolEnum } from '@/shared/domain/rol.enum';

@Injectable()
export class UsuarioService {
  constructor(
    @InjectRepository(Usuario)
    private readonly usuarioRepository: Repository<Usuario>,
  ) {}

  async obtenerDocente(docenteId: number): Promise<Usuario> {
    return await this.usuarioRepository.findOne({
      relations: ['rol', 'perfil'],
      where: { id: docenteId, rol: { id: RolEnum.DOCENTE } },
    });
  }

  async obtenerDocentes(): Promise<Usuario[]> {
    return await this.usuarioRepository.find({
      relations: ['rol', 'perfil'],
      where: { rol: { id: RolEnum.DOCENTE } },
    });
  }
}
