import { Inject, Injectable } from '@nestjs/common';
import config from '@/config';
import { ConfigType } from '@nestjs/config';
import { HttpService } from '@nestjs/axios';
import { lastValueFrom } from 'rxjs';
import { InjectRepository } from '@nestjs/typeorm';
import { RelacionNeural } from '@/shared/entities/relacion-neural.entity';
import { Repository } from 'typeorm';

@Injectable()
export class RelacionNeuralService {
  private url: string;
  private token =
    'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU0ZjBkYTQ2ZmQzZjM1ZDkyYWE0ZTQ5ZWM3ODZjZjM2YmU2ZTk3OWY4NWZhZjNlMWU5NTc0ODRiNTNkMzkzOTA0ZDY3MmFjYzc5YTY1MTc3In0.eyJhdWQiOiIxIiwianRpIjoiNTRmMGRhNDZmZDNmMzVkOTJhYTRlNDllYzc4NmNmMzZiZTZlOTc5Zjg1ZmFmM2UxZTk1NzQ4NGI1M2QzOTM5MDRkNjcyYWNjNzlhNjUxNzciLCJpYXQiOjE2NTgzNzIwNzksIm5iZiI6MTY1ODM3MjA3OSwiZXhwIjoxNjg5OTA4MDc5LCJzdWIiOiIzMyIsInNjb3BlcyI6W119.NcEsaBeGR0t32-Pd97QPVR6hpRXM_FJJV4sL9VCKgvrBLQWxP6EdRjk1fh3SKBHBcP1Gve9etbhe1GDh-Oa_tTSA6IP3hsfg2Vovzw73wVbS7h7abTZ0pl7KT5h-L1536MUllHJmaV6wawF0oYSBLiBvNQQAY0yNd5OBtfIBolkcOq4RNHoNO1f8ckeR5aGsnQ6sgi6DXgpVRjhIZkw5SRbd510JfxLirnwPZWiNlPyH011wTZgOzp295WQTcKQjO0DmoAdpeMdH70fhtbr4wwrNNEWeAYPVh4RqxSru_I3qftPj4SkDtpa6L_P3Gc-BExpB7NXvqX_-F0TW7s88lVkFSmyAcdeAJqMrrRUhGquTg1AwmXMbI7fze3P2rP_ZQMU51gq9KLOuYZEMPzKUFPtcpzb4IyOWqBXbnmeVh-XISZOrUdCQ8B2eo5uRGery--1lseBxffhsIZSctPw8sn3xjws7giOAyOv-z03taC_EdG5KJ6rmTCpRmwFQ5sLBlCCKSMmlggttW5975_Oig9l6o80E6PSCtJ8Q89xvIbHav1ckBz8SrBhvjhdlwYKH3-s_VqGBggAAJp9aqTwzvvIknDnHrrIjE8O07WqGpOUyd8zq0-fJKnc3kzzmaNxvShPd-iUGTvXCcb7xdKPXaAbUgkdxBkcHrnlLkH8UQPg';

  constructor(
    @Inject(config.KEY)
    private readonly configService: ConfigType<typeof config>,
    private readonly http: HttpService,
    @InjectRepository(RelacionNeural)
    private readonly relacionNeuralRepository: Repository<RelacionNeural>,
  ) {
    this.url = `${configService.neuralactions.url}/edge`;
  }

  async crearRelacion(nodoInicial: string, nodoFinal: string, nombre: string) {
    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${this.token}`,
    };
    const data = {
      node_a: nodoInicial,
      node_b: nodoFinal,
      relation_type: nombre,
    };
    try {
      const response = await lastValueFrom(
        this.http.post(`${this.url}`, data, { headers }),
      );
      return response.data['id'];
    } catch (error) {
      console.log(error);
    }
  }

  async guardarRelacion(id: string, nodoInicial: string, nodoFinal: string) {
    const relacion = this.relacionNeuralRepository.create({
      id,
      nodoInicial,
      nodoFinal,
    });
    return await this.relacionNeuralRepository.save(relacion);
  }

  async actualizar(
    nodoInicial: string,
    nodoFinal: string,
    nombre: string,
  ): Promise<RelacionNeural> {
    const { id } = await this.relacionNeuralRepository.findOne({
      where: { nodoInicial, nodoFinal },
    });
    await this.eliminar(id);
    const nuevoId = await this.crearRelacion(nodoInicial, nodoFinal, nombre);
    await this.relacionNeuralRepository.delete(id);
    return await this.relacionNeuralRepository.save({
      id: nuevoId,
      nodoInicial,
      nodoFinal,
    });
  }

  async eliminar(id: string) {
    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${this.token}`,
    };
    try {
      const response = await lastValueFrom(
        this.http.delete(`${this.url}`, { headers, data: { id } }),
      );
      return response.data['id'];
    } catch (error) {
      console.log(error);
    }
  }

  async eliminarRelaciones(nodoInicial: string) {
    const relaciones = await this.relacionNeuralRepository.findBy({
      nodoInicial,
    });
    return await this.relacionNeuralRepository.delete(
      relaciones.map(({ id }) => id),
    );
  }
}
