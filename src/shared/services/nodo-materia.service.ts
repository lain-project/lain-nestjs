import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { NodoMateria } from '@/shared/entities/nodo-materia.entity';
import { Repository } from 'typeorm';

@Injectable()
export class NodoMateriaService {
  constructor(
    @InjectRepository(NodoMateria)
    private readonly nodoMateriaRepository: Repository<NodoMateria>,
  ) {}

  async obtenerNodoMateria(
    materiaId: string,
    nombre: string,
  ): Promise<NodoMateria> {
    return await this.nodoMateriaRepository.findOne({
      relations: ['materia'],
      where: { materia: { id: materiaId }, nombre },
    });
  }
}
