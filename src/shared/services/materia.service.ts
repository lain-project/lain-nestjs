import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Materia } from '@/shared/entities/materia.entity';
import { Not, Repository } from 'typeorm';

@Injectable()
export class MateriaService {
  constructor(
    @InjectRepository(Materia)
    private readonly materiaRepository: Repository<Materia>,
  ) {}

  async getMaterias(): Promise<Materia[]> {
    return await this.materiaRepository.find({
      relations: ['anioPlanEstudio'],
    });
  }
  async getMateriasAnteriores(materiaId: string): Promise<Materia[]> {
    const materia = await this.materiaRepository.findOne({
      where: { id: materiaId },
      relations: [
        'anioPlanEstudio',
        'anioPlanEstudio.planDeEstudio',
        'anioPlanEstudio.anio',
      ],
    });
    const materias = await this.materiaRepository.find({
      where: {
        anioPlanEstudio: {
          planDeEstudio: { id: materia.anioPlanEstudio.planDeEstudio.id },
        },
        id: Not(materiaId),
      },
      relations: [
        'anioPlanEstudio',
        'anioPlanEstudio.planDeEstudio',
        'anioPlanEstudio.anio',
      ],
    });

    return materias.filter(
      (mat) =>
        mat.anioPlanEstudio.anio.numeration <=
        materia.anioPlanEstudio.anio.numeration,
    );
  }

  async findOne(materiaId: string): Promise<Materia> {
    return await this.materiaRepository.findOne({
      where: { id: materiaId },
      relations: ['anioPlanEstudio', 'anioPlanEstudio.planDeEstudio'],
    });
  }
}
