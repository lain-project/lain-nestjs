import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Esquema } from '@/shared/entities/esquema.entity';
import { Repository } from 'typeorm';

@Injectable()
export class EsquemaService {
  constructor(
    @InjectRepository(Esquema)
    private readonly esquemaRepository: Repository<Esquema>,
  ) {}

  async findByPlanDeEstudio(
    planDeEstudioId: number,
    name: string,
  ): Promise<Esquema> {
    return await this.esquemaRepository.findOne({
      relations: ['planDeEstudio'],
      where: { planDeEstudio: { id: planDeEstudioId }, nombre: name },
    });
  }
}
