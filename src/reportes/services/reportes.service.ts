import { Injectable } from '@nestjs/common';
import { Fechas } from '@/reportes/dto/fechas';
import { InjectRepository } from '@nestjs/typeorm';
import { Pregunta } from '@/reportes/entities/pregunta.entity';
import { Between, Repository } from 'typeorm';
import { stopwords } from '@/reportes/domain/stopwords';

@Injectable()
export class ReportesService {
  constructor(
    @InjectRepository(Pregunta, 'secondaryDB')
    private preguntaRepository: Repository<Pregunta>,
  ) {}

  async totalDePreguntas(fechas: Fechas) {
    const preguntas = await this.preguntaRepository.find({
      where: { fecha: Between(fechas.inicio, fechas.fin) },
    });

    const total = preguntas.length;
    const intenciones = Array.from(this.obtenerIntenciones(preguntas));
    const intencionesContadas = this.contarIntenciones(preguntas, intenciones);
    const palabrasRepeticion = this.cantidadRepeticiones(preguntas);
    const palabrasPorFecha = this.palabrasPorFecha(preguntas);

    return {
      total,
      intencionesContadas,
      palabrasRepeticion,
      palabrasPorFecha,
    };
  }

  obtenerIntenciones(preguntas: Pregunta[]): Set<string> {
    return new Set(
      preguntas.map(
        ({ intencionDetectada }) =>
          intencionDetectada?.split('.')[0] || 'noDetectada',
      ),
    );
  }

  contarIntenciones(preguntas: Pregunta[], intenciones: string[]) {
    const intencionesPreguntas = preguntas.map(
      ({ intencionDetectada }) =>
        intencionDetectada?.split('.')[0] || 'noDetectada',
    );
    const inicio = {};
    intenciones.forEach((intencion) => (inicio[intencion] = 0));

    const totalIntenciones = intencionesPreguntas.reduce(
      (contador, intencion) => {
        contador[intencion] = contador[intencion] + 1;
        return contador;
      },
      inicio,
    );
    return totalIntenciones;
  }

  cantidadRepeticiones(preguntasDataset: Pregunta[]) {
    const preguntas = preguntasDataset
      .map(({ pregunta }) =>
        pregunta
          .toLowerCase()
          .replace(/[?,.!]/, '')
          .trim(),
      )
      .filter((pregunta) => !stopwords.includes(pregunta));

    const keys = Array.from(new Set(preguntas));
    const results = keys.reduce((prev, key) => {
      prev[key] = 0;
      return prev;
    }, {});

    preguntas.forEach((pregunta) => (results[pregunta] += 1));

    return results;
  }

  palabrasPorFecha(preguntasDataset: Pregunta[]) {
    const fechas = preguntasDataset.map(({ fecha }) =>
      this.convertirFecha(fecha),
    );
    const fechasKey = Array.from(new Set(fechas));
    const preguntasConFecha = preguntasDataset
      .map(({ pregunta, fecha }) => ({
        pregunta: pregunta
          .toLowerCase()
          .replace(/[?,.!]/, '')
          .trim(),
        fecha: this.convertirFecha(fecha),
      }))
      .filter((pregunta) => !stopwords.includes(pregunta.pregunta));

    const inicio = {};
    fechasKey.forEach((fecha) => (inicio[fecha] = []));
    const palabrasFecha = preguntasConFecha.reduce(
      (contador, preguntaFecha) => {
        contador[preguntaFecha.fecha].push(preguntaFecha.pregunta);
        return contador;
      },
      inicio,
    );
    const result = Object.keys(palabrasFecha).map((key) => ({
      fecha: key,
      palabras: this.contarRepeticiones(palabrasFecha[key]),
    }));

    return result.sort(
      (a: any, b: any) =>
        new Date(a.fecha).getTime() - new Date(b.fecha).getTime(),
    );
  }

  convertirFecha(date: Date) {
    const join = (t, a, s) => {
      function format(m) {
        const f = new Intl.DateTimeFormat('en', m);
        return f.format(t);
      }
      return a.map(format).join(s);
    };

    const a = [{ day: 'numeric' }, { month: 'numeric' }, { year: 'numeric' }];
    return join(date, a, '-');
  }

  contarRepeticiones(palabras: string[]) {
    const keys = Array.from(new Set(palabras));
    const inicial = {};
    keys.forEach((key) => (inicial[key] = 0));
    return palabras.reduce((result, palabra) => {
      result[palabra] += 1;
      return result;
    }, inicial);
  }
}
