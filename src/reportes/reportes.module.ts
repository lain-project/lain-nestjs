import { Module } from '@nestjs/common';
import { ReportesController } from './controllers/reportes.controller';
import { ReportesService } from './services/reportes.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Pregunta } from '@/reportes/entities/pregunta.entity';

@Module({
  controllers: [ReportesController],
  providers: [ReportesService],
  imports: [TypeOrmModule.forFeature([Pregunta], 'secondaryDB')],
})
export class ReportesModule {}
