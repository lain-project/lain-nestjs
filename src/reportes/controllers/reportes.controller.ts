import {Body, Controller, Get, Post, UseGuards} from '@nestjs/common';
import { JwtAuthGuard } from '@/auth/guards/jwt-auth.guard';
import { RolesGuard } from '@/auth/guards/roles.guard';
import { ApiTags } from '@nestjs/swagger';
import { Roles } from '@/auth/decorators/roles.decorator';
import { Rol } from '@/auth/models/rol.enum';
import { ReportesService } from '@/reportes/services/reportes.service';
import { Fechas } from '@/reportes/dto/fechas';

@UseGuards(JwtAuthGuard, RolesGuard)
@ApiTags('Reportes')
@Controller('reportes')
export class ReportesController {
  constructor(private readonly reporteService: ReportesService) {}

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Post('total-preguntas')
  async totalDePreguntas(@Body() fechas: Fechas) {
    return await this.reporteService.totalDePreguntas(fechas);
  }
}
