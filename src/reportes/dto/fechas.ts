import {IsDate, IsOptional, IsString} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class Fechas {
  @IsDate()
  @IsOptional()
  @ApiProperty()
  inicio: Date;

  @IsDate()
  @IsOptional()
  @ApiProperty()
  fin: Date;
}
