import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('preguntas', { database: 'secondaryDB' })
export class Pregunta {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { name: 'pregunta' })
  pregunta: string;

  @Column('varchar', { name: 'intencion_detectada' })
  intencionDetectada: string;

  @Column('varchar', { name: 'respuesta' })
  respuesta: string;

  @Column('timestamp', { name: 'fecha' })
  fecha: Date;

  @Column('varchar', { name: 'numero' })
  telefono: string;
}
