import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Audit } from '@/shared/entities/audit.entity';
import { Materia } from '@/shared/entities/materia.entity';

@Entity('correlativas')
export class Correlativa extends Audit {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { name: 'nombre' })
  nombre: string;

  @Column('varchar', { name: 'id_relacion_neural' })
  idRelacionNeural: string;

  @Column('int', { name: 'cursar' })
  necesariaParaCursar: number;

  @Column('int', { name: 'final' })
  necesariaParaRendirFinal: number;

  @Column('boolean', { name: 'estado', default: true })
  estado: boolean;

  @ManyToOne(() => Materia)
  @JoinColumn({ name: 'materia_inicial' })
  correlativa: Materia;

  @ManyToOne(() => Materia)
  @JoinColumn({ name: 'materia_final' })
  materia: Materia;
}
