export enum CorrelativaCursar {
  NO_APLICA = 0,
  REGULARIZADA = 1,
  APROBADA = 2,
}

export enum CorrelativaFinal {
  NO_APLICA = 0,
  APROBADA = 1,
}

export enum CorrelativaNombres {
  REGULARIZADA_CURSAR = 'Regularizada',
  REGULARIZADA_Y_APROBADA = 'Regularizada y aprobada para rendir',
  APROBADA_CURSAR = 'Aprobada para cursar',
  APROBADA_CURSAR_APROBADA_FINAL = 'Aprobada para cursar y rendir',
  APROBADA_FINAL = 'Aprobada para rendir',
}
