import { Injectable } from '@nestjs/common';
import { CorrelativaACrear } from '@/correlativa/dto/correlativa.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Correlativa } from '@/correlativa/entities/correlativa.entity';
import { Repository } from 'typeorm';
import { MateriaService } from '@/shared/services/materia.service';
import {
  CorrelativaCursar,
  CorrelativaFinal,
  CorrelativaNombres,
} from '@/correlativa/entities/correlativa-estado.enum';
import { CorrelativaNeuralactionsService } from '@/correlativa/services/correlativa-neuralactions.service';

@Injectable()
export class CorrelativaService {
  constructor(
    @InjectRepository(Correlativa)
    private readonly correlativaRepository: Repository<Correlativa>,
    private readonly correlativaNeuralactionsService: CorrelativaNeuralactionsService,
    private readonly materiaService: MateriaService,
  ) {}

  async asignarCorrelativa(correlativaACrear: CorrelativaACrear) {
    const correlativa = await this.materiaService.findOne(
      correlativaACrear.correlativaId,
    );
    const materia = await this.materiaService.findOne(
      correlativaACrear.materiaId,
    );

    const nombre = this.armadoDeNombre(
      correlativaACrear.necesariaParaCursar,
      correlativaACrear.necesariaParaRendirFinal,
    );

    const relacionCorrelativa = this.correlativaRepository.create({
      ...correlativaACrear,
      materia,
      correlativa,
      nombre,
    });

    const idRelacionNeural = await this.correlativaNeuralactionsService.guardar(
      relacionCorrelativa,
    );

    relacionCorrelativa.idRelacionNeural = idRelacionNeural;
    return await this.correlativaRepository.save(relacionCorrelativa);
  }

  private armadoDeNombre(cursar: CorrelativaCursar, final: CorrelativaFinal) {
    const REGULARIZADA_CURSAR =
      cursar === CorrelativaCursar.REGULARIZADA &&
      final === CorrelativaFinal.NO_APLICA;
    const REGULARIZADA_Y_APROBADA =
      cursar === CorrelativaCursar.REGULARIZADA &&
      final === CorrelativaFinal.APROBADA;
    const APROBADA_CURSAR =
      cursar === CorrelativaCursar.APROBADA &&
      final === CorrelativaFinal.NO_APLICA;
    const APROBADA_CURSAR_APROBADA_FINAL =
      cursar === CorrelativaCursar.APROBADA &&
      final === CorrelativaFinal.APROBADA;
    const APROBADA_FINAL =
      cursar === CorrelativaCursar.NO_APLICA &&
      final === CorrelativaFinal.APROBADA;

    if (REGULARIZADA_CURSAR) return CorrelativaNombres.REGULARIZADA_CURSAR;
    if (REGULARIZADA_Y_APROBADA)
      return CorrelativaNombres.REGULARIZADA_Y_APROBADA;
    if (APROBADA_CURSAR) return CorrelativaNombres.APROBADA_CURSAR;
    if (APROBADA_CURSAR_APROBADA_FINAL)
      return CorrelativaNombres.APROBADA_CURSAR_APROBADA_FINAL;
    if (APROBADA_FINAL) return CorrelativaNombres.APROBADA_FINAL;

    return '';
  }

  async obtenerCorrelativas(materiaId: string): Promise<Correlativa[]> {
    return this.correlativaRepository.find({
      where: { materia: { id: materiaId }, estado: true },
      relations: ['materia', 'correlativa'],
    });
  }

  async eliminarCorrelativa(id: string) {
    const correlativa = await this.correlativaRepository.findOne({
      where: { id },
    });
    const result = await this.correlativaNeuralactionsService.eliminar(
      correlativa,
    );
    console.log(result);
    correlativa.estado = false;
    return await this.correlativaRepository.save(correlativa);
  }

  async obtenerMaterias(materiaId: string) {
    return await this.materiaService.getMateriasAnteriores(materiaId);
  }
}
