import { Injectable } from '@nestjs/common';
import { Correlativa } from '@/correlativa/entities/correlativa.entity';
import { NeuralactionsService } from '@/shared/services/neuralactions.service';
import { EsquemaService } from '@/shared/services/esquema.service';
import { NodoMateriaService } from '@/shared/services/nodo-materia.service';
import { RelacionNeuralService } from '@/shared/services/relacion-neural.service';

@Injectable()
export class CorrelativaNeuralactionsService {
  constructor(
    private readonly neuralactions: NeuralactionsService,
    private readonly esquemaService: EsquemaService,
    private readonly nodoMateriaService: NodoMateriaService,
    private readonly relacionNeuralService: RelacionNeuralService,
  ) {}

  async guardar(correlativa: Correlativa) {
    const nodoInicial = correlativa.correlativa.nodoId;
    const nodoFinal = correlativa.materia.nodoId;

    return await this.relacionNeuralService.crearRelacion(
      nodoInicial,
      nodoFinal,
      correlativa.nombre,
    );
  }

  async eliminar(correlativa: Correlativa) {
    return await this.relacionNeuralService.eliminar(
      correlativa.idRelacionNeural,
    );
  }
}
