import { Module } from '@nestjs/common';
import { CorrelativaController } from './controllers/correlativa.controller';
import { CorrelativaService } from './services/correlativa.service';
import { CorrelativaNeuralactionsService } from './services/correlativa-neuralactions.service';
import { SharedModule } from '@/shared/shared.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Materia } from '@/shared/entities/materia.entity';
import { Correlativa } from '@/correlativa/entities/correlativa.entity';

@Module({
  controllers: [CorrelativaController],
  providers: [CorrelativaService, CorrelativaNeuralactionsService],
  imports: [SharedModule, TypeOrmModule.forFeature([Correlativa, Materia])],
})
export class CorrelativaModule {}
