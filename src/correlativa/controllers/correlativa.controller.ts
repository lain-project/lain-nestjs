import {Body, Controller, Delete, Get, Param, Post, UseGuards} from '@nestjs/common';
import { Roles } from '@/auth/decorators/roles.decorator';
import { Rol } from '@/auth/models/rol.enum';
import { CorrelativaService } from '@/correlativa/services/correlativa.service';
import { CorrelativaACrear } from '@/correlativa/dto/correlativa.dto';
import { JwtAuthGuard } from '@/auth/guards/jwt-auth.guard';
import { RolesGuard } from '@/auth/guards/roles.guard';
import { ApiTags } from '@nestjs/swagger';

@UseGuards(JwtAuthGuard, RolesGuard)
@ApiTags('Correlativas')
@Controller('correlativa')
export class CorrelativaController {
  constructor(private readonly correlativasService: CorrelativaService) {}

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Post('')
  async asignarCorrelativa(@Body() correlativaACrear: CorrelativaACrear) {
    return await this.correlativasService.asignarCorrelativa(correlativaACrear);
  }

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Get('/:materiaId')
  async obtenerCorrelativa(@Param('materiaId') materiaId: string) {
    return await this.correlativasService.obtenerCorrelativas(materiaId);
  }

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Delete('/:correlativaId')
  async eliminarCorrelativa(@Param('correlativaId') correlativaId: string) {
    return await this.correlativasService.eliminarCorrelativa(correlativaId);
  }

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Get('/materias/:materiaId')
  async obtenerMateriasCorrelativas(@Param('materiaId') materiaId: string) {
    return await this.correlativasService.obtenerMaterias(materiaId);
  }
}
