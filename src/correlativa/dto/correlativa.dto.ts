import { NeuralactionsToken } from '@/shared/dtos/neuralactions-token';
import { IsEnum, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import {
  CorrelativaCursar,
  CorrelativaFinal,
} from '@/correlativa/entities/correlativa-estado.enum';

export class CorrelativaACrear extends NeuralactionsToken {
  @IsString()
  @ApiProperty()
  correlativaId: string;

  @IsString()
  @ApiProperty()
  materiaId: string;

  @IsEnum(CorrelativaCursar)
  @ApiProperty()
  necesariaParaCursar: CorrelativaCursar;

  @IsEnum(CorrelativaFinal)
  @ApiProperty()
  necesariaParaRendirFinal: CorrelativaFinal;
}
