import { registerAs } from '@nestjs/config';

export default registerAs('config', () => ({
  database: {
    host: process.env.DATABASE_HOST,
    name: process.env.DATABASE_NAME,
    password: process.env.DATABASE_PASSWORD,
    port: parseInt(process.env.DATABASE_PORT),
    user: process.env.DATABASE_USER,
  },
  neuralactions: {
    url: process.env.NEURALACTIONS_URL,
  },
}));
