import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Bibliografia } from '@/bibliografia/entities/bibliografia.entity';
import { Repository } from 'typeorm';
import {
  BibliografiaAActualizar,
  BibliografiaACrear,
} from '@/bibliografia/dto/bibliografia.dto';
import { MateriaService } from '@/shared/services/materia.service';
import { BibliografiaNeuralactionsService } from '@/bibliografia/services/bibliografia-neuralactions.service';

@Injectable()
export class BibliografiaService {
  constructor(
    @InjectRepository(Bibliografia)
    private readonly bibliografiaRepository: Repository<Bibliografia>,
    private readonly bibliografiaNeuralactionsService: BibliografiaNeuralactionsService,
    private readonly materiaService: MateriaService,
  ) {}

  async crearBibliografia(bibliografiaACrear: BibliografiaACrear) {
    const materia = await this.materiaService.findOne(
      bibliografiaACrear.materiaId,
    );
    const bibliografia = await this.bibliografiaRepository.create({
      ...bibliografiaACrear,
      materia,
    });

    const nodoId = await this.bibliografiaNeuralactionsService.guardar(
      materia,
      bibliografia,
    );

    const relacion =
      await this.bibliografiaNeuralactionsService.relacionarBibliografia(
        nodoId,
        materia,
      );

    bibliografia.nodoId = nodoId;

    await this.bibliografiaRepository.save(bibliografia);

    return !!relacion;
  }

  async obtenerBibliografia(materiaId: string) {
    return await this.bibliografiaRepository.find({
      where: {
        estado: true,
        materia: { id: materiaId },
      },
    });
  }

  async actualizarBibliografia(bibliografiaAActual: BibliografiaAActualizar) {
    let bibliografia = await this.bibliografiaRepository.findOne({
      where: { id: bibliografiaAActual.id, estado: true },
      relations: ['materia'],
    });

    if (!bibliografia) {
      throw new BadRequestException(
        `No existe bibliografia con el id ${bibliografiaAActual.id}`,
      );
    }

    bibliografia = this.bibliografiaRepository.merge(
      bibliografia,
      bibliografiaAActual,
    );

    const materia = await this.materiaService.findOne(
      bibliografiaAActual.materiaId,
    );

    await this.bibliografiaNeuralactionsService.actualizar(
      bibliografia,
      materia,
    );
    bibliografia = await this.bibliografiaRepository.save(bibliografia);

    return !!bibliografia;
  }

  async desasignarBibliografia(bibliografiaId: string) {
    const bibliografia = await this.bibliografiaRepository.findOneBy({
      id: bibliografiaId,
    });
    await this.bibliografiaNeuralactionsService.eliminar(bibliografia);
    bibliografia.estado = false;
    return await this.bibliografiaRepository.save(bibliografia);
  }
}
