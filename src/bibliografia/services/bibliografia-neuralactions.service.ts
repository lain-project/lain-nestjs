import { Injectable } from '@nestjs/common';
import { NeuralactionsService } from '@/shared/services/neuralactions.service';
import { EsquemaService } from '@/shared/services/esquema.service';
import { NodoMateriaService } from '@/shared/services/nodo-materia.service';
import { RelacionNeuralService } from '@/shared/services/relacion-neural.service';
import { Materia } from '@/shared/entities/materia.entity';
import { Bibliografia } from '@/bibliografia/entities/bibliografia.entity';
import { Nodo } from '@/shared/domain/nodo.model';

@Injectable()
export class BibliografiaNeuralactionsService {
  constructor(
    private readonly neuralactions: NeuralactionsService,
    private readonly esquemaService: EsquemaService,
    private readonly nodoMateriaService: NodoMateriaService,
    private readonly relacionNeuralService: RelacionNeuralService,
  ) {}

  async guardar(materia: Materia, bibliografia: Bibliografia) {
    const { planDeEstudio } = materia.anioPlanEstudio;
    const { esquemaId } = await this.esquemaService.findByPlanDeEstudio(
      planDeEstudio.id,
      'bibliografia',
    );
    const nodo: Nodo = {
      values: {
        name: bibliografia.nombre,
        nombre: bibliografia.nombre,
        isbn: bibliografia.isbn,
        autor: bibliografia.autor,
        scheme_id: esquemaId,
        hue: 21,
      },
      workspaces: {
        active_workspace_id: planDeEstudio.workspaceId,
      },
    };
    return await this.neuralactions.crearNodo(nodo);
  }

  async relacionarBibliografia(nodoBibliografiaId: any, materia: Materia) {
    const { nodoId } = await this.nodoMateriaService.obtenerNodoMateria(
      materia.id,
      'Bibliografia',
    );
    const idRelacion = await this.relacionNeuralService.crearRelacion(
      nodoBibliografiaId,
      nodoId,
      'contiene',
    );

    return await this.relacionNeuralService.guardarRelacion(
      idRelacion,
      nodoBibliografiaId,
      nodoId,
    );
  }

  async actualizar(bibliografia: Bibliografia, materia: Materia) {
    const { planDeEstudio } = materia.anioPlanEstudio;
    const { esquemaId } = await this.esquemaService.findByPlanDeEstudio(
      planDeEstudio.id,
      'bibliografia',
    );
    const nodo: Nodo = {
      id: bibliografia.nodoId,
      values: {
        name: bibliografia.nombre,
        nombre: bibliografia.nombre,
        isbn: bibliografia.isbn,
        autor: bibliografia.autor,
        scheme_id: esquemaId,
        hue: 21,
      },
      workspaces: {
        active_workspace_id: planDeEstudio.workspaceId,
      },
    };
    return await this.neuralactions.actualizarNodo(nodo);
  }

  async eliminar(bibliografia: Bibliografia) {
    return await this.neuralactions.eliminarNodo(bibliografia.nodoId);
  }
}
