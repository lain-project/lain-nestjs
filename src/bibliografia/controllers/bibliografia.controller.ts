import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { BibliografiaService } from '@/bibliografia/services/bibliografia.service';
import { Roles } from '@/auth/decorators/roles.decorator';
import { Rol } from '@/auth/models/rol.enum';
import {
  BibliografiaAActualizar,
  BibliografiaACrear,
} from '@/bibliografia/dto/bibliografia.dto';
import { JwtAuthGuard } from '@/auth/guards/jwt-auth.guard';
import { RolesGuard } from '@/auth/guards/roles.guard';
import { ApiTags } from '@nestjs/swagger';

@UseGuards(JwtAuthGuard, RolesGuard)
@ApiTags(`Bibliografia`)
@Controller('bibliografia')
export class BibliografiaController {
  constructor(private readonly bibliografiaService: BibliografiaService) {}

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Get('/:materiaId')
  async obtenerBibliografias(@Param('materiaId') materiaId: string) {
    return await this.bibliografiaService.obtenerBibliografia(materiaId);
  }

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Post('/')
  async asignarBibliografia(@Body() bibliografiaACrear: BibliografiaACrear) {
    return this.bibliografiaService.crearBibliografia(bibliografiaACrear);
  }

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Put('/')
  async actualizarBibliografia(
    @Body() bibliografiaAActual: BibliografiaAActualizar,
  ) {
    return await this.bibliografiaService.actualizarBibliografia(
      bibliografiaAActual,
    );
  }

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Delete('/:bibliografiaId')
  async desasignarBibliografia(
    @Param('bibliografiaId') bibliografiaId: string,
  ) {
    return await this.bibliografiaService.desasignarBibliografia(
      bibliografiaId,
    );
  }
}
