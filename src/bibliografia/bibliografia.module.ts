import { Module } from '@nestjs/common';
import { BibliografiaService } from './services/bibliografia.service';
import { BibliografiaController } from './controllers/bibliografia.controller';
import { BibliografiaNeuralactionsService } from './services/bibliografia-neuralactions.service';
import { SharedModule } from '@/shared/shared.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Bibliografia } from '@/bibliografia/entities/bibliografia.entity';

@Module({
  providers: [BibliografiaService, BibliografiaNeuralactionsService],
  controllers: [BibliografiaController],
  imports: [SharedModule, TypeOrmModule.forFeature([Bibliografia])],
})
export class BibliografiaModule {}
