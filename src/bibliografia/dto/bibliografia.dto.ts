import { NeuralactionsToken } from '@/shared/dtos/neuralactions-token';
import { IsOptional, IsString } from 'class-validator';
import {ApiProperty, PartialType} from '@nestjs/swagger';

export class BibliografiaACrear extends NeuralactionsToken {
  @IsString()
  @ApiProperty()
  nombre: string;

  @IsString()
  @ApiProperty()
  isbn: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  autor: string;

  @IsString()
  @ApiProperty()
  materiaId: string;
}

export class BibliografiaAActualizar extends PartialType(BibliografiaACrear) {
  @IsString()
  @IsOptional()
  @ApiProperty()
  id: string;
}
