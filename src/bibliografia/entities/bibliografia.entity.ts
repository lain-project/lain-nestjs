import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Audit } from '@/shared/entities/audit.entity';
import { Materia } from '@/shared/entities/materia.entity';

@Entity('bibliografias')
export class Bibliografia extends Audit {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { name: 'nombre' })
  nombre: string;

  @Column('varchar', { name: 'isbn' })
  isbn: string;

  @Column('varchar', { name: 'autor' })
  autor: string;

  @Column('boolean', { name: 'estado', default: true })
  estado: boolean;

  @Column('varchar', { name: 'nodo_id' })
  nodoId: string;

  @ManyToOne(() => Materia)
  @JoinColumn({ name: 'materia_id' })
  materia: Materia;
}
