import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Audit } from '@/shared/entities/audit.entity';
import { Materia } from '@/shared/entities/materia.entity';

@Entity('unidades')
export class Unidad extends Audit {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('int', { name: 'numero' })
  numero: number;

  @Column('varchar', { name: 'nombre' })
  nombre: string;

  @Column('varchar', { name: 'objetivos' })
  objetivos: string;

  @Column('boolean', { name: 'estado', default: true })
  estado: boolean;

  @Column('varchar', { name: 'nodo_id' })
  nodoId: string;

  @ManyToOne(() => Materia)
  @JoinColumn({ name: 'materia_id' })
  materia: Materia;
}
