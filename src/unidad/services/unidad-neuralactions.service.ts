import { Injectable } from '@nestjs/common';
import { Unidad } from '@/unidad/entities/unidad.entity';
import { Materia } from '@/shared/entities/materia.entity';
import { EsquemaService } from '@/shared/services/esquema.service';
import { Nodo } from '@/shared/domain/nodo.model';
import { NeuralactionsService } from '@/shared/services/neuralactions.service';
import { NodoMateriaService } from '@/shared/services/nodo-materia.service';
import { RelacionNeuralService } from '@/shared/services/relacion-neural.service';

@Injectable()
export class UnidadNeuralactionsService {
  constructor(
    private readonly neuralactions: NeuralactionsService,
    private readonly esquemaService: EsquemaService,
    private readonly nodoMateriaService: NodoMateriaService,
    private readonly relacionNeuralService: RelacionNeuralService,
  ) {}

  async guardarUnidad(unidad: Unidad, materia: Materia) {
    const planDeEstudio = materia.anioPlanEstudio.planDeEstudio;
    const esquemaUnidad = await this.esquemaService.findByPlanDeEstudio(
      planDeEstudio.id,
      'unidad',
    );
    const nodo: Nodo = {
      values: {
        nombre: unidad.nombre,
        name: unidad.nombre,
        numero: unidad.numero.toString(),
        objetivos: unidad.objetivos,
        scheme_id: esquemaUnidad.esquemaId,
        hue: 210,
      },
      workspaces: {
        active_workspace_id: planDeEstudio.workspaceId,
      },
    };
    return await this.neuralactions.crearNodo(nodo);
  }

  async relacionarUnidadAUnidades(nodoUnidadId: string, materia: Materia) {
    const nodoUnidades = await this.nodoMateriaService.obtenerNodoMateria(
      materia.id,
      'Unidades',
    );
    const idRelacion = await this.relacionNeuralService.crearRelacion(
      nodoUnidadId,
      nodoUnidades.nodoId,
      'es unidad',
    );

    return await this.relacionNeuralService.guardarRelacion(
      idRelacion,
      nodoUnidadId,
      nodoUnidades.nodoId,
    );
  }

  async actualizarUnidad(unidad: Unidad, materia: Materia): Promise<string> {
    const { planDeEstudio } = materia.anioPlanEstudio;
    const esquemaUnidad = await this.esquemaService.findByPlanDeEstudio(
      planDeEstudio.id,
      'unidad',
    );

    const nodo: Nodo = {
      id: unidad.nodoId,
      values: {
        nombre: unidad.nombre,
        name: unidad.nombre,
        numero: unidad.numero.toString(),
        objetivos: unidad.objetivos,
        hue: 210,
        scheme_id: esquemaUnidad.esquemaId,
      },
      workspaces: {
        active_workspace_id: planDeEstudio.workspaceId,
      },
    };
    return await this.neuralactions.actualizarNodo(nodo);
  }

  async desasignarUnidad(nodoId: string): Promise<any> {
    return await this.neuralactions.eliminarNodo(nodoId);
  }
}
