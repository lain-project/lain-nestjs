import { BadRequestException, Injectable } from '@nestjs/common';
import { UnidadAActualizar, UnidadACrear } from '@/unidad/dto/unidad.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Unidad } from '@/unidad/entities/unidad.entity';
import { Repository } from 'typeorm';
import { MateriaService } from '@/shared/services/materia.service';
import { UnidadNeuralactionsService } from '@/unidad/services/unidad-neuralactions.service';

@Injectable()
export class UnidadService {
  constructor(
    @InjectRepository(Unidad)
    private readonly unidadRepository: Repository<Unidad>,
    private readonly unidadNeuralactionsService: UnidadNeuralactionsService,
    private readonly materiaService: MateriaService,
  ) {}

  async asignarUnidad(unidadACrear: UnidadACrear): Promise<boolean> {
    if (
      await this.existeUnidadRepetida(
        unidadACrear.materiaId,
        unidadACrear.numero,
      )
    ) {
      throw new BadRequestException(
        `Ya existe una unidad con el numero ${unidadACrear.numero}`,
      );
    }

    const materia = await this.materiaService.findOne(unidadACrear.materiaId);
    const unidad: Unidad = await this.unidadRepository.create({
      ...unidadACrear,
      materia,
    });
    const nodoId = await this.unidadNeuralactionsService.guardarUnidad(
      unidad,
      materia,
    );

    unidad.nodoId = nodoId;

    await this.unidadRepository.save(unidad);

    const relacion =
      await this.unidadNeuralactionsService.relacionarUnidadAUnidades(
        nodoId,
        materia,
      );

    return !!relacion;
  }

  private async existeUnidadRepetida(
    materiaId: string,
    numero: number,
  ): Promise<Unidad> {
    const unidadesDeMateria = await this.unidadRepository.find({
      where: { materia: { id: materiaId }, estado: true },
      relations: ['materia'],
    });

    return unidadesDeMateria.find((unidad) => unidad.numero === numero);
  }

  async obtenerUnidades(materiaId: string): Promise<Unidad[]> {
    return await this.unidadRepository.find({
      where: {
        estado: true,
        materia: { id: materiaId },
      },
      order: {
        numero: 'asc',
      },
    });
  }

  async actualizarUnidad(unidadAActualizar: UnidadAActualizar) {
    let unidad = await this.unidadRepository.findOne({
      where: { id: unidadAActualizar.id, estado: true },
      relations: ['materia'],
    });

    if (!unidad) {
      throw new BadRequestException(
        `No existe unidad con el id ${unidadAActualizar.id}`,
      );
    }

    const unidadRepetida = await this.existeUnidadRepetida(
      unidadAActualizar.materiaId,
      unidadAActualizar.numero,
    );

    if (!!unidadRepetida && unidadRepetida.id !== unidad.id) {
      throw new BadRequestException(
        `Ya existe una unidad con el numero ${unidadAActualizar.numero}`,
      );
    }
    unidad = this.unidadRepository.merge(unidad, unidadAActualizar);

    const materia = await this.materiaService.findOne(
      unidadAActualizar.materiaId,
    );
    await this.unidadNeuralactionsService.actualizarUnidad(unidad, materia);
    unidad = await this.unidadRepository.save(unidad);

    return !!unidad;
  }

  async desasignarUnidad(id: string) {
    const unidad = await this.unidadRepository.findOneBy({ id });
    await this.unidadNeuralactionsService.desasignarUnidad(unidad.nodoId);
    unidad.estado = false;
    return await this.unidadRepository.save(unidad);
  }

  async obtenerUnidad(id: string): Promise<Unidad> {
    return await this.unidadRepository.findOne({
      where: { id },
      relations: [
        'materia',
        'materia.anioPlanEstudio',
        'materia.anioPlanEstudio.planDeEstudio',
      ],
    });
  }
}
