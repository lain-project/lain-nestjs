import { Module } from '@nestjs/common';
import { UnidadController } from './controllers/unidad.controller';
import { UnidadService } from './services/unidad.service';
import { UnidadNeuralactionsService } from './services/unidad-neuralactions.service';
import { SharedModule } from '@/shared/shared.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Unidad } from '@/unidad/entities/unidad.entity';
import { Materia } from '@/shared/entities/materia.entity';

@Module({
  controllers: [UnidadController],
  providers: [UnidadService, UnidadNeuralactionsService],
  imports: [SharedModule, TypeOrmModule.forFeature([Unidad, Materia])],
  exports: [UnidadService],
})
export class UnidadModule {}
