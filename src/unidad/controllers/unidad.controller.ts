import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '@/auth/guards/jwt-auth.guard';
import { RolesGuard } from '@/auth/guards/roles.guard';
import { ApiTags } from '@nestjs/swagger';
import { Roles } from '@/auth/decorators/roles.decorator';
import { Rol } from '@/auth/models/rol.enum';
import { UnidadAActualizar, UnidadACrear } from '@/unidad/dto/unidad.dto';
import { UnidadService } from '@/unidad/services/unidad.service';

@UseGuards(JwtAuthGuard, RolesGuard)
@ApiTags('Unidades')
@Controller('unidad')
export class UnidadController {
  constructor(private readonly unidadService: UnidadService) {}

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Post('')
  async asignarUnidad(@Body() unidadACrear: UnidadACrear) {
    return await this.unidadService.asignarUnidad(unidadACrear);
  }

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Get('/:materiaId')
  async obtenerUnidades(@Param('materiaId') materiaId: string) {
    return await this.unidadService.obtenerUnidades(materiaId);
  }

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Put()
  async actualizarUnidad(@Body() unidadAActualizar: UnidadAActualizar) {
    return await this.unidadService.actualizarUnidad(unidadAActualizar);
  }

  @Roles(Rol.ADMIN, Rol.DOCENTE)
  @Delete('/:unidadId')
  async desasignarUnidad(@Param('unidadId') unidadId: string) {
    return await this.unidadService.desasignarUnidad(unidadId);
  }
}
