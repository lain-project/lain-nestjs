import { NeuralactionsToken } from '@/shared/dtos/neuralactions-token';
import { IsNumber, IsOptional, IsPositive, IsString } from 'class-validator';
import {ApiProperty, PartialType} from '@nestjs/swagger';

export class UnidadACrear extends NeuralactionsToken {
  @IsString()
  @IsOptional()
  @ApiProperty()
  nombre: string;

  @IsNumber()
  @IsPositive()
  @IsOptional()
  @ApiProperty()
  numero: number;

  @IsString()
  @IsOptional()
  @ApiProperty()
  objetivos: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  materiaId: string;
}

export class UnidadAActualizar extends PartialType(UnidadACrear){
  @IsString()
  @IsOptional()
  @ApiProperty()
  id: string;
}
