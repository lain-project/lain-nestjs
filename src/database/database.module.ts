import { Global, Module } from '@nestjs/common';
import config from '@/config';
import { ConfigType } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import {Pregunta} from "@/reportes/entities/pregunta.entity";

@Global()
@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      inject: [config.KEY],
      useFactory: (configService: ConfigType<typeof config>) => {
        const { user, password, port, host, name } = configService.database;
        return {
          type: 'postgres',
          autoLoadEntities: true,
          host,
          port,
          username: user,
          password,
          database: name,
          entities: [],
        };
      },
    }),
    TypeOrmModule.forRootAsync({
      inject: [config.KEY],
      name: 'secondaryDB',
      useFactory: (configService: ConfigType<typeof config>) => {
        return {
          type: 'cockroachdb',
          url: process.env.DATABASE_LAIN_CHAT,
          ssl: true,
          extra: {
            options: '--cluster=silky-piglet-5571',
            application_name: 'lain-chats',
          },
          entities: [Pregunta],
        };
      },
    }),
  ],
  exports: [TypeOrmModule],
})
export class DatabaseModule {}
